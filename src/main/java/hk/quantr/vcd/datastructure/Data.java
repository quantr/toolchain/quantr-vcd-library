package hk.quantr.vcd.datastructure;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Data {

	public String left;
	public String right;

	public Data(String left, String right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return this.left + (this.left.contains("b") ? " " : "") + (this.right != null ? this.right : "") + '\n';
	}

}
