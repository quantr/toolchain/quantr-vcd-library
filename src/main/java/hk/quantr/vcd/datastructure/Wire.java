package hk.quantr.vcd.datastructure;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Wire implements Cloneable {

	public String name;
	public String size;
	public String reference;
	public String type;
	public ArrayList<BigInteger> values = new ArrayList<>();
	public transient Scope scope;

	public Wire() {
	}

	public Wire(String name, String size, String reference, String type, Scope scope) {
		this.name = name;
		this.size = size;
		this.reference = reference;
		this.type = type;
		this.scope = scope;
	}

	@Override
	public String toString() {
		return "$var wire " + size + ' ' + reference + ' ' + name + (Integer.parseInt(size) > 1 ? " [" + (Integer.parseInt(size) - 1) + ":0]" : "") + " $end\n";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Wire newWire = (Wire) super.clone();
		newWire.values = (ArrayList<BigInteger>) this.values.clone();
		return newWire;
	}
}
