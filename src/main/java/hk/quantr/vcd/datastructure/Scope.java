package hk.quantr.vcd.datastructure;

//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Scope {

	public String name;
	public ArrayList<Wire> wires = new ArrayList<>();
	public ArrayList<Scope> scopes = new ArrayList<>();
//	private Set<String> added = new HashSet<>();

	public Scope() {
	}

	public Scope(String name, JSONArray wires, JSONArray scopes, Set<String> added) {
		this.name = name;
//		this.added = added;
		for (int i = 0; i < wires.size(); i++) {
			this.wires.add(new Wire((String) ((JSONObject) wires.get(i)).get("name"),
					(String) ((JSONObject) wires.get(i)).get("size"),
					(String) ((JSONObject) wires.get(i)).get("reference"),
					(String) ((JSONObject) wires.get(i)).get("type"),
					this));
		}
		loopScopes(scopes, added);
	}

	public void loopScopes(JSONArray scopes, Set<String> added) {
		for (int i = 0; i < scopes.size(); i++) {
			if (!added.contains((String) ((JSONObject) scopes.get(i)).get("name"))) {
				added.add((String) ((JSONObject) scopes.get(i)).get("name"));
				this.scopes.add(new Scope((String) ((JSONObject) scopes.get(i)).get("name"),
						(JSONArray) ((JSONObject) scopes.get(i)).get("wires"),
						(JSONArray) ((JSONObject) scopes.get(i)).get("scopes"),
						added));
				loopScopes((JSONArray) ((JSONObject) scopes.get(i)).get("scopes"), added);
			}
		}
	}

	public String scopesToString() {
		String str = "";
		for (Scope s : this.scopes) {
			str += s.toString();
		}
		return str;
	}

	public String wiresToString() {
		String str = "";
		for (Wire w : wires) {
			str += w;
		}
		return str;
	}

	@Override
	public String toString() {
		return "$scope module " + name + " $end\n" + wiresToString() + scopesToString() + "$upscope $end\n";
	}
}
