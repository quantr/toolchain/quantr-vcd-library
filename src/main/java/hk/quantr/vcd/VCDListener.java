/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.vcd;

import hk.quantr.vcd.antlr.QuantrVCDParser;
import hk.quantr.vcd.antlr.QuantrVCDParserBaseListener;
import hk.quantr.vcd.datastructure.Data;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.VCD;
import hk.quantr.vcd.datastructure.Wire;
import java.util.Stack;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VCDListener extends QuantrVCDParserBaseListener {

	public VCD vcd = new VCD();
	Stack<Scope> stack = new Stack<>();

	@Override
	public void exitScope_start(QuantrVCDParser.Scope_startContext ctx) {
		Scope tempScope = new Scope();
		if (vcd.scope == null) {
			vcd.scope = tempScope;
		}
		tempScope.name = ctx.IDENTIFIER().getText();
		stack.push(tempScope);
	}

	@Override
	public void exitScope_end(QuantrVCDParser.Scope_endContext ctx) {
		Scope tempScope = stack.pop();
		if (stack.size() > 0) {
			stack.peek().scopes.add(tempScope);
		}
	}

	@Override
	public void exitWire(QuantrVCDParser.WireContext ctx) {
		Wire wire = new Wire();
		wire.name = ctx.reference().getText();
		wire.size = ctx.size().getText();
		wire.reference = ctx.var_identifier().getText();
		wire.scope = stack.peek();
		stack.peek().wires.add(wire);
	}

	@Override
	public void exitData(QuantrVCDParser.DataContext ctx) {
		String value = ctx.getText();
//		String left = null;
//		if (ctx.IDENTIFIER(0) != null) {
//			left = ctx.IDENTIFIER(0).getText();
//		} else if (ctx.NUMBER(0) != null) {
//			left = ctx.NUMBER(0).getText();
//		}
//
//		String right = null;
//		if (ctx.IDENTIFIER(1) != null) {
//			right = ctx.IDENTIFIER(1).getText();
//		} else if (ctx.NUMBER() != null) {
//			right = ctx.NUMBER().getText();
//		}
		Data data = new Data(ctx.left, ctx.right);
		vcd.data.add(data);
	}

}
