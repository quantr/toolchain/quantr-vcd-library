lexer grammar QuantrVCDLexer;

WS		:	(' '|'\t')+ -> skip;


NL		:	'\r'? '\n';
SCOPE		:	'$scope';
UPSCOPE		:	'$upscope';
MODULE		:	'module';
VERSION		:	'$version';
DATE		:	'$date';
TIMESCALE	:	'$timescale';
VAR			:	'$var';
WIRE		:	'wire';
END			:	'$end';
ENDDEFINITIONS	:	'$enddefinitions';
TIMEUNIT	:	[0-9] 'ns';
NUMBER		:	[0-9]+;
//ARRAY		:	'[' [0-9]+ COLON [0-9]+ ']';
IDENTIFIER		:	~('\r'|'\n'|' '|'\t')+;


COLON	:	':';
SEMICOLON	:	';';
PLUS	:	'+';
HASH	:	'#';


