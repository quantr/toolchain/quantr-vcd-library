parser grammar QuantrVCDParser;

options { tokenVocab=QuantrVCDLexer; }

@header {
}

@parser::members{
}

parse	:	structure EOF
		;

structure	:	version? date? timescale scope enddefinitions data+
			;

version	:	VERSION NUMBER? IDENTIFIER IDENTIFIER IDENTIFIER END NL+
		;

date	:	DATE IDENTIFIER IDENTIFIER NUMBER IDENTIFIER NUMBER NL? END NL+
		;

timescale	:	TIMESCALE (TIMEUNIT|IDENTIFIER) END NL+
			;

scope		:	scope_start scope_statement* scope_end NL+
			;

scope_start	:	SCOPE MODULE IDENTIFIER END NL+
			;

scope_end	:	UPSCOPE END
			;

scope_statement	:	wire
				|	scope
				;

wire		:	VAR var_type size var_identifier reference IDENTIFIER? END NL+
			;

var_type	:	WIRE;

size		:	NUMBER;

var_identifier	:	(IDENTIFIER|NUMBER);

reference	:	IDENTIFIER;

enddefinitions	:	ENDDEFINITIONS	END NL+
				;

data	returns[String left, String right]:	
		l=IDENTIFIER r=IDENTIFIER? NL+	{$left=$l.text; $right=$r.text;}
		|	l=IDENTIFIER r=NUMBER? NL+		{$left=$l.text; $right=$r.text;}
		|	l=NUMBER r=NUMBER? NL+			{$left=$l.text; $right=$r.text;}
		;
