package hk.quantr.vcd;

import com.google.gson.Gson;
import hk.quantr.javalib.CommonLib;
import hk.quantr.quantrverilogtool.Output;
import hk.quantr.quantrverilogtool.QuantrVerilogTool;
import hk.quantr.vcd.antlr.QuantrVCDLexer;
import hk.quantr.vcd.antlr.QuantrVCDParser;
import hk.quantr.vcd.datastructure.Data;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.VCD;
import hk.quantr.vcd.datastructure.Wire;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrVCDLibrary {

	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static void main(String args[]) throws FileNotFoundException, IOException {
		if (args.length == 0) {
			System.out.println("please pass xml file name");
			System.exit(1);
		}
		String fileContent = IOUtils.toString(new FileInputStream(args[0]), "utf-8");
		System.out.println(convertVCDToJson(new File(args[0]).getParentFile(), fileContent, true));
	}

	public static String convertVCDToJson(File folder, String content, boolean hasSign) {
		QuantrVCDLexer lexer = new QuantrVCDLexer(CharStreams.fromString(content, "utf8"));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		QuantrVCDParser parser = new QuantrVCDParser(tokenStream);
//		parser.removeErrorListeners();
//		parser.addErrorListener(new UnderlineListener());
		VCDListener vcdListener = new VCDListener();
		parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
		parser.addParseListener(vcdListener);
		QuantrVCDParser.ParseContext context = parser.parse();

//		dump(vcdListener.scope, 1);
//		XmlMapper xmlMapper = new XmlMapper();
//		String xml = xmlMapper.writeValueAsString(vcdListener.vcd);
//		System.out.println(CommonLib.prettyFormatXml(xml, 4));
		Gson gson = new Gson();
		VCD vcd = vcdListener.vcd;
		HashMap<String, Output> verilogModules = processAllVerilogFiles(folder);
		processData(vcd, hasSign);
		processWireType(vcd.scope, verilogModules);
		String json = gson.toJson(vcd);
		return CommonLib.prettyFormatJson(json);
	}

	public static String convertJsonToVCD(File file, String timescale) throws IOException, ParseException {
		Object obj = new JSONParser().parse(new FileReader(file));
		JSONObject jo = (JSONObject) obj;

		Set<String> added = new HashSet<>();
		String str = "$timescale " + timescale + " $end\n";
		JSONObject _scope = (JSONObject) jo.get("scope");
		Scope scope = new Scope((String) _scope.get("name"),
				(JSONArray) _scope.get("wires"),
				(JSONArray) _scope.get("scopes"),
				added);
		str += scope;
		str += "$enddefinitions $end\n";
		JSONArray _data = (JSONArray) jo.get("data");
		ArrayList<Data> data = new ArrayList<Data>();
		for (int i = 0; i < _data.size(); i++) {
			Data d = new Data((String) ((JSONObject) _data.get(i)).get("left"), (String) ((JSONObject) _data.get(i)).get("right"));
			data.add(d);
			str += d;
		}
		return str;
	}

	public static VCD convertVCDToObject(File folder, String content, boolean hasSign) {
//		System.out.println("1> " + format.format(new Date()));
		QuantrVCDLexer lexer = new QuantrVCDLexer(CharStreams.fromString(content, "utf8"));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

//		System.out.println("1.1> " + format.format(new Date()));
		QuantrVCDParser parser = new QuantrVCDParser(tokenStream);
//		parser.removeErrorListeners();
//		parser.addErrorListener(new UnderlineListener());
		VCDListener vcdListener = new VCDListener();
		parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
		parser.addParseListener(vcdListener);
//		System.out.println("1.2> " + format.format(new Date()));
		QuantrVCDParser.ParseContext context = parser.parse();
		VCD vcd = vcdListener.vcd;
//		System.out.println("1.3> " + format.format(new Date()));
		HashMap<String, Output> verilogModules = processAllVerilogFiles(folder);

//		System.out.println("2> " + format.format(new Date()));
		processData(vcd, hasSign);

//		System.out.println("3> " + format.format(new Date()));
		processWireType(vcd.scope, verilogModules);

//		System.out.println("4> " + format.format(new Date()));
		return vcd;
	}

	private static HashMap<String, Output> processAllVerilogFiles(File folder) {
//		try {
//			ExecutorService executorService = Executors.newFixedThreadPool(16);
		HashMap<String, Output> verilogModules = new HashMap<>();
		if (folder != null) {
			Collection<File> files = FileUtils.listFiles(folder, new String[]{"v"}, false);
//			CountDownLatch latch = new CountDownLatch(files.size());
			for (File f : files) {
//				executorService.submit(() -> {
				try {
					String content = IOUtils.toString(new FileInputStream(f), "utf-8");
					Output output = QuantrVerilogTool.getOutput(content);
					verilogModules.put(output.moduleName, output);
//						latch.countDown();
//						System.out.println("a> " + f.getName() + " = " + format.format(new Date()));
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
//				});
			}
		}
//			latch.await();
		return verilogModules;
//		} catch (InterruptedException ex) {
//			Logger.getLogger(QuantrVCDLibrary.class.getName()).log(Level.SEVERE, null, ex);
//		}
//		return null;
	}

	public static void processWireType(Scope scope, HashMap<String, Output> verilogModules) {
		if (verilogModules.size() == 0) {
			return;
		}
		Output verilogModule = verilogModules.get(scope.name);
		if (verilogModule != null) {
			for (Wire wire : scope.wires) {
				if (verilogModule.inputs.contains(wire.name)) {
					wire.type = "input";
				} else if (verilogModule.outputs.contains(wire.name)) {
					wire.type = "output";
				}
			}
		}
		for (Scope s : scope.scopes) {
			processWireType(s, verilogModules);
		}
	}

	private static void processData(VCD vcd, boolean hasSign) {
		ArrayList<Wire> wires = new ArrayList<>();
		getAllWires(vcd.scope, wires);
		HashMap<String, String> wireValues = new HashMap<>();

		for (Wire wire : wires) {
			wireValues.put(wire.reference, "0");
		}

		ArrayList<RowData> rowsData = new ArrayList<>();
		for (int i = 0; i < vcd.data.size(); i++) {
			if (vcd.data.get(i).left.charAt(0) == '#') {
				rowsData.add(new RowData(vcd.data.get(i).left, new ArrayList<Data>()));
			} else {
				rowsData.get(rowsData.size() - 1).data.add(vcd.data.get(i));
			}
		}

		for (int x = 0; x < rowsData.size(); x++) {
			for (int z = 0; z < rowsData.get(x).data.size(); z++) {
				Data data = rowsData.get(x).data.get(z);
				String reference;
				String val;
				if (data.right == null) {
					reference = data.left.substring(1);
					val = data.left.substring(0, 1);
				} else {
					reference = data.right;
					val = data.left;
				}

				wireValues.put(reference, val);
			}

			for (Wire wire : wires) {
				String val = wireValues.get(wire.reference);
//				System.out.println(wire.reference + " = " + val);
				if (val.charAt(0) == 'b') {
					if (hasSign) {
						wire.values.add(BigInteger.valueOf(new BigInteger(val.substring(1), 2).longValue()));
					} else {
						wire.values.add(new BigInteger(val.substring(1), 2));
					}
				} else {
					wire.values.add(new BigInteger(val));
				}

			}
		}
	}

	public static void dump(Scope scope, int indent) {
		System.out.println("-".repeat(indent) + " " + scope);
		if (scope == null) {
			return;
		}
		for (Wire wire : scope.wires) {
			System.out.println("-".repeat(indent) + " " + wire);
		}
		for (Scope s : scope.scopes) {
			dump(s, indent + 1);
		}
	}

	public static void getAllWires(Scope scope, ArrayList<Wire> wires) {
		if (scope == null) {
			return;
		}
		for (Wire wire : scope.wires) {
			wires.add(wire);
		}
		for (Scope s : scope.scopes) {
			getAllWires(s, wires);
		}
	}

	public static void getAllLeafWires(Scope scope, ArrayList<Wire> wires) {
		if (scope == null) {
			return;
		}
		if (scope.scopes.isEmpty()) {
			for (Wire wire : scope.wires) {
				wires.add(wire);
			}
		}

		for (Scope s : scope.scopes) {
			getAllWires(s, wires);
		}
	}

}
