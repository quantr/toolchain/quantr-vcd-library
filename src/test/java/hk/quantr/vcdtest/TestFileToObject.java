/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.vcdtest;

import hk.quantr.vcd.QuantrVCDLibrary;
import static hk.quantr.vcd.QuantrVCDLibrary.convertJsonToVCD;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.parser.ParseException;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestFileToObject {

	@Test
	public void test() throws FileNotFoundException, IOException, ParseException {
		File vcdFile = new File("src/test/resources/conversion.vcd");
		FileWriter fileWriter1 = new FileWriter(vcdFile);
		String content = convertJsonToVCD(new File(getClass().getResource("/test2.json").getFile()), "1ps");
//		System.out.println(content);
		fileWriter1.write(content);
		fileWriter1.close();

		File jsonFile = new File("src/test/resources/conversion.json");
		FileWriter fileWriter2 = new FileWriter(jsonFile);
		String json = QuantrVCDLibrary.convertVCDToJson(new File(""), content, true);
//		System.out.println(json);
		fileWriter2.write(json);
		fileWriter2.close();
	}
}
