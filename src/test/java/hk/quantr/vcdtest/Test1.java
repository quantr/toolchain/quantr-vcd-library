/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.vcdtest;

import static hk.quantr.vcd.QuantrVCDLibrary.convertVCDToJson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	@Test
	public void test() throws FileNotFoundException, IOException {
//		String fileContent = IOUtils.toString(new FileInputStream("/Users/peter/workspace/quantr-i/output.vcd"), "utf-8");
		String fileContent = IOUtils.toString(new FileInputStream("C:\\Users\\User\\Documents\\NetBeansProjects\\quantr-vcd-library\\src\\test\\resources\\output.vcd"), "utf-8");
		System.out.println(convertVCDToJson(new File("C:\\Users\\User\\Documents\\NetBeansProjects"), fileContent, true));
	}
}
