package hk.quantr.vcdtest;

import com.google.gson.Gson;
import hk.quantr.javalib.CommonLib;
import hk.quantr.vcd.VCDListener;
import hk.quantr.vcd.antlr.QuantrVCDLexer;
import hk.quantr.vcd.antlr.QuantrVCDParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ConvertToJson {

	@Test
	public void test() throws Exception {
		QuantrVCDLexer lexer = new QuantrVCDLexer(CharStreams.fromString(IOUtils.toString(getClass().getResourceAsStream("/cpu4.vcd"), "utf8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		QuantrVCDParser parser = new QuantrVCDParser(tokenStream);
//		parser.removeErrorListeners();
//		parser.addErrorListener(new UnderlineListener());
		VCDListener vcdListener = new VCDListener();
		parser.addParseListener(vcdListener);
		QuantrVCDParser.ParseContext context = parser.parse();

//		dump(vcdListener.scope, 1);
//		XmlMapper xmlMapper = new XmlMapper();
//		String xml = xmlMapper.writeValueAsString(vcdListener.vcd);
//		System.out.println(CommonLib.prettyFormatXml(xml, 4));
		Gson gson = new Gson();
		String json = gson.toJson(vcdListener.vcd);
		System.out.println(CommonLib.prettyFormatJson(json));
	}

}
