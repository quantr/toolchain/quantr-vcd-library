package hk.quantr.vcdtest;

import hk.quantr.vcd.VCDListener;
import hk.quantr.vcd.antlr.QuantrVCDLexer;
import hk.quantr.vcd.antlr.QuantrVCDParser;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.Wire;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestParser {

	@Test
	public void test() throws Exception {
		QuantrVCDLexer lexer = new QuantrVCDLexer(CharStreams.fromString(IOUtils.toString(getClass().getResourceAsStream("/cpu4.vcd"), "utf8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		QuantrVCDParser parser = new QuantrVCDParser(tokenStream);
//		parser.removeErrorListeners();
//		parser.addErrorListener(new UnderlineListener());
		VCDListener vcdListener = new VCDListener();
		parser.addParseListener(vcdListener);
		QuantrVCDParser.ParseContext context = parser.parse();

		dump(vcdListener.vcd.scope, 1);
	}

	private void dump(Scope scope, int indent) {
		System.out.println("-".repeat(indent) + " " + scope);
		for (Wire wire : scope.wires) {
			System.out.println("-".repeat(indent) + " " + wire);
		}
		for (Scope s : scope.scopes) {
			dump(s, indent + 1);
		}
	}
}
