# Introduction

a Java library to read VCD format https://en.wikipedia.org/wiki/Value_change_dump

# Author

Peter, System Architect <peter@quantr.hk>

# Usage

## CLI

```
java -jar quantr-vcd-library-1.1.jar output.vcd
```

![](https://www.quantr.foundation/wp-content/uploads/2021/11/Screenshot_4.png)

### In Java

```
String fileContent = IOUtils.toString(new FileInputStream("c:\\workspace\\quantr-i\\output.vcd"), "utf-8");
System.out.println(convertVCDToJson(fileContent));
```


